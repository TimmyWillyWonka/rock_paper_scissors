const getConfig = require('startupjs/bundler.cjs').webpackWebConfig

module.exports = getConfig(undefined, {
  forceCompileModules: [
    '@dmapper/auth'
  ],
  alias: {},
  mode: 'react-native'
})

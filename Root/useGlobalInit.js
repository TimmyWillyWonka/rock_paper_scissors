import { Platform } from 'react-native'
import { useModel, useSession, useDoc, useSyncEffect } from 'startupjs'

export default function useGlobalInit (serverSession) {
  let $session = useModel('_session')

  // Initialize _session on mobile
  useSyncEffect(() => {
    if (serverSession) $session.setEach(serverSession)
  }, [])

  // Perform global app state initialization
  let [userId] = useSession('userId')
  // need on native mobile for 'if' below
  const [loggedIn = Platform.OS !== 'web'] = useSession('loggedIn')
  let [user, $user] = useDoc('users', userId)
  // we need to skip this condition for landing page (using loggedIn)
  if (!user && loggedIn) return null

  useSyncEffect(() => {
    // Fix cycle references when user is undefined
    if (user) $session.ref('user', $user)
  }, [])

  return true
}

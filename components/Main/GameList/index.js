import React, { useState } from 'react'
import {
  observer,
  emit,
  useBatchQuery,
  useBatch, useSession,
  $root
} from 'startupjs'
import { Image, ScrollView } from 'react-native'
import './index.styl'
import { Div, Span, Alert } from '@startupjs/ui'
import { BASE_URL } from '@env'

export default observer(function GameList () {
  const [user] = useSession('user')
  const [games, $games] = useBatchQuery('games', {})
  useBatch()
  const [showAlert, setShowAlert] = useState()

  const joinToRoom = async gameId => {
    const $game = $root.at('games.' + gameId)
    await $game.subscribe()
    const result = await $game.joinToGame(user)
    $game.unsubscribe()
    if (!result) {
      setShowAlert(!showAlert)
      setTimeout(() => {
        setShowAlert(false)
      }, 2000)
      return
    }
    emit('url', '/room/' + gameId)
  }

  return pug`
    ScrollView.root
      Span.title Game List:
      if games.length
        Div.gamesBlock
          each game in games
            Div.item(onPress=()=> joinToRoom(game.id))
              Span.gameName= game.name
              if game.creatorId === user.id
                Span.roomInfo (My room)
                Div.deleteItemBlock(onPress=() => $games.at(game.id).deleteGame())
                  Image.deleteImage(source={ uri: BASE_URL + '/icons/delete.png' })
          if showAlert
            Div.alertBlock
              Alert(
                color='warning'
                label='There are no places'
              )
      else
        Div.noGameBlock
          Span.blockText No games there
  `
})

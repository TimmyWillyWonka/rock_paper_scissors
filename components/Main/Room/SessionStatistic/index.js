import React from 'react'
import { observer } from 'startupjs'
import { Div, Span } from '@startupjs/ui'
import { ScrollView } from 'react-native'
import './index.styl'

export default observer(function SessionStatistic ({ firstPlayer, secondPlayer }) {
  if (!(firstPlayer.history && secondPlayer.history)) return null
  const games = [...firstPlayer.history, ...secondPlayer.history]
  return pug`
    ScrollView.root
      Span.title Game history:
      if firstPlayer.history && secondPlayer.history
        Div.container
          each item, index in games
            Div.statisticContainer
              Span.text Round: #{index + 1}
              Span.text Win: #{item.name}
              Span.text Score: #{item.score}
`
})

import React, { useEffect } from 'react'
import {
  observer,
  useSession,
  useBatchDoc,
  useBatch,
  useQueryDoc,
  emit,
  useSyncEffect,
  $root
} from 'startupjs'
import { Image } from 'react-native'
import './index.styl'
import { Div, Span, Button } from '@startupjs/ui'
import { BASE_URL } from '@env'
import SessionStatistic from './SessionStatistic'

const DATA = { icons: ['/icons/rock.png', '/icons/paper.png', '/icons/scissors.png'] }

export default observer(function Room ({ gameId }) {
  const [userId] = useSession('userId')

  const [game, $game] = useBatchDoc('games', gameId)
  const isOpponentUserId = game && game.playerIds.find(id => id !== userId)
  const [user] = useBatchDoc('users', isOpponentUserId || '__DUMMY__')
  useBatch()

  const [firstPlayer = {}, $firstPlayer] = useQueryDoc('players', { userId: game && game.playerIds[0] })
  const [secondPlayer = {}, $secondPlayer] = useQueryDoc('players', { userId: game && game.playerIds[1] })
  const [player = {}, $player] = useQueryDoc('players', { userId, gameId })

  useEffect(() => {
    if (firstPlayer.choice && secondPlayer.choice) {
      gameLogic(firstPlayer.choice, secondPlayer.choice)
    }
  }, [player.choice])

  useSyncEffect(() => {
    if (!game) {
      emit('url', '/')
    }
  }, [JSON.stringify(game)])
  if (!game) return null

  const isCreator = game && game.creatorId === userId
  const disabled = game.playerIds.length < 2 || game.start || game.gameOver
  const startGame = game.start
  const disabledItem = player && player.move

  const switchRound = () => {
    $firstPlayer.set('choice', 0)
    $firstPlayer.set('move', false)
    $firstPlayer.set('roundWinner', false)

    $secondPlayer.set('choice', 0)
    $secondPlayer.set('move', false)
    $secondPlayer.set('roundWinner', false)

    $game.increment('round')
  }

  const getWinnerName = () => {
    if (firstPlayer.score > secondPlayer.score) $firstPlayer.set('isWinner', true)
    if (firstPlayer.score < secondPlayer.score) $secondPlayer.set('isWinner', true)

    if (firstPlayer.isWinner) {
      return firstPlayer.name
    }
    if (secondPlayer.isWinner) {
      return secondPlayer.name
    }
  }

  const gameLogic = (argOne, argTwo) => {
    const states = {
      rock: { paper: -1, rock: 0, scissors: 1 },
      paper: { paper: 0, rock: 1, scissors: -1 },
      scissors: { paper: 1, rock: -1, scissors: 0 }
    }
    if (states[argOne][argTwo]) {
      if (states[argOne][argTwo] > 0) {
        $firstPlayer.increment('combo')
        $secondPlayer.set('combo', 0)
        if (firstPlayer.combo > 1) {
          $firstPlayer.set('score', firstPlayer.score * 2)
        } else $firstPlayer.increment('score')
        $firstPlayer.push('history', {
          name: firstPlayer.name,
          score: firstPlayer.score,
          ts: +Date.now()
        })
        $firstPlayer.set('roundWinner', true)
      } else {
        $secondPlayer.increment('combo')
        $firstPlayer.set('combo', 0)
        if (secondPlayer.combo > 1) {
          $secondPlayer.set('score', secondPlayer.score * 2)
        } else $secondPlayer.increment('score')
        $secondPlayer.push('history', {
          name: secondPlayer.name,
          score: secondPlayer.score,
          ts: +Date.now()
        })
        $firstPlayer.set('roundWinner', true)
      }
    }
  }

  const getGameValue = value => {
    switch (value) {
      case 0:
        return 'rock'
      case 1:
        return 'paper'
      case 2:
        return 'scissors'
    }
  }

  const userLost = async () => {
    const $isOpponent = $root.query('players', { userId: isOpponentUserId, gameId })
    await $isOpponent.subscribe()
    const [isOpponent] = $isOpponent.get()
    const $winnerUser = $root.at(`players.${isOpponent.id}`)
    $winnerUser.set('isWinner', true)
    $game.set('gameOver', true)
    $isOpponent.unsubscribe()
  }

  return pug`
    Div.root
      if isCreator
        Div.gameControlContainer
          Div.userActivesBlock
            Span.lable Users to room:
            Span.info= game.playerIds.length + ' / 2' || 0 + ' / 2'
          Div.buttonContainer
            Button.creatorButton(onPress=() => emit('url', '/')) Back
            Button.creatorButton(
              onPress=() => $game.set('start', +new Date())
              disabled=disabled
            ) Start game
          Div.winnerBlock
            if firstPlayer.isWinner || secondPlayer.isWinner
              Span.winnerTitle Winner:
              Span.winnerText= getWinnerName()
        if startGame
          Div.controlBlock
            Span.round Round
            Span.number= game.round
          Div.controlButtonBlock
            Button.btn(
              onPress= () => {
                $game.set('finish', +Date.now())
                getWinnerName()
              }
              disabled=firstPlayer.isWinner || secondPlayer.isWinner
            ) Finish game
            Button.btn(
              onPress=switchRound
              disabled=!(firstPlayer.choice && secondPlayer.choice) || game.gameOver || (firstPlayer.isWinner || secondPlayer.isWinner)
            ) Next round
        Div.statisticBlock
          SessionStatistic(firstPlayer=firstPlayer secondPlayer=secondPlayer)
      else
        if game.playerIds.length < 2
          Div.waitingForPlayersBlock
            Span.warningText Expect an opponent...
            Div.userActivesBlock
              Span.lable Users to room:
              Span.info= game.playerIds.length + ' / 2' || 0 + ' / 2'
        else
          Div.opponentBlock
            Span.lable Plays against you:
            Span.info= user && user.name
          unless startGame
            Span.wait Please wait until the professor starts the game.
          Div.throwInTheTowelBlock
            Button.throwInTheTowelButton(onPress=() => emit('url', '/')) Back
            Button.throwInTheTowelButton(onPress=userLost disabled=firstPlayer.isWinner || secondPlayer.isWinner) I lost
          Div.userWinnerBlock
            if firstPlayer.isWinner || secondPlayer.isWinner
              Span.winnerTitle Winner:
              Span.winnerText= getWinnerName()
          Div.statisticBlock
            SessionStatistic(firstPlayer=firstPlayer secondPlayer=secondPlayer)
          Div.userChoiceBlock
            Span
          if startGame
            Div.itemsBlock
              each icon, index in DATA.icons
                Div.container(
                  onPress=() => {
                    $player.set('choice', getGameValue(index)),
                    $player.set('move', true)
                  }
                  disabled=disabledItem
                )
                  Image.icon(source={ uri: BASE_URL + icon })
  `
})

import React, { useState, useEffect } from 'react'
import { observer, emit, useSession, useBatchDoc, useBatchQuery, useBatch } from 'startupjs'
import './index.styl'
import { Div, Span, Checkbox } from '@startupjs/ui'
import GameList from './GameList'
import _uniqBy from 'lodash/uniqBy'

export default observer(function Main () {
  const [userId] = useSession('userId')
  const [isCreatorMode, setIsCreatorMode] = useState()
  const [, $user] = useBatchDoc('users', userId)
  const [sessionResults] = useBatchQuery('sessionResults', {
    $sort: { scores: -1 },
    $limit: 3
  })
  useBatch()

  useEffect(() => {
    if (isCreatorMode) $user.set('isCreator', true)
    else $user.set('isCreator', false)
  }, [isCreatorMode])

  const freeArray = _uniqBy(sessionResults, 'userId')

  return pug`
    Div.root
      Checkbox(
        label='I am a professor'
        value=isCreatorMode
        onChange=() =>setIsCreatorMode(!isCreatorMode)
      )
      if isCreatorMode
        Div.buttonBlock(onPress=() => emit('url', '/create'))
          Span.buttonName Create Game!
      Div.gameListBlock
        GameList(userId)
      if sessionResults
        Div.statisticUsersBlock
          Span.title Leaders:
          each leader in freeArray
            Div.item
              Span.playerName= leader.playerName
              Span.playerScores= leader.scores

  `
})

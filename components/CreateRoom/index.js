import React, { useState } from 'react'
import { observer, emit, useModel, useSession } from 'startupjs'
import './index.styl'
import { Div, Span, Button, Input } from '@startupjs/ui'

export default observer(function CreateRoom () {
  const [name, setName] = useState('')
  const [userId] = useSession('userId')
  const $games = useModel('games')

  const disabled = !name.length

  const createUserRoom = async () => {
    const gameId = await $games.addGameRoom({
      name,
      round: 0,
      playerIds: [],
      creatorId: userId
    })
    emit('url', '/room/' + gameId)
  }

  return pug`
    Div.root
      Span.info Enter game name
      Div.row
        Input.inputField( 
          type='text'
          placeholder='Game name...'
          value=name
          onChangeText=setName
        )
      Div.buttonContainer
        Button.setButton(onPress=() => emit('url', '/')) Back
        Button.setButton(onPress=createUserRoom disabled=disabled) Create room
  `
})

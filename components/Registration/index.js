import React, { useState } from 'react'
import { observer, useValue } from 'startupjs'
import './index.styl'
import { Platform } from 'react-native'
import { Div, Span, Input, Button } from '@startupjs/ui'
import validate from 'validate.js'
import validationSchema from './validationSchema'
import axios from 'axios'

export default observer(function Registration ({ setState, onFinish }) {
  const [errors, setErrors] = useState({})
  const [data, $data] = useValue({
    userName: '',
    email: '',
    password: ''
  })

  const isWeb = Platform.OS === 'web'
  const register = async () => {
    setErrors({})

    let err = validate(
      data,
      validationSchema
    )
    if (err) {
      return setErrors(err)
    }

    const result = await axios
      .post('/auth/register', {
        email: data.email,
        password: data.password,
        confirm: data.password,
        userData: {
          name: data.userName,
          isCreator: false
        }
      })
      .catch(async err => {
        return err
      })

    if (!result.data) return alert(result.response.data.message)
    const userId = result.data
    if (userId) {
      await axios.post('/auth/local', {
        email: data.email.toLowerCase(),
        password: data.password
      })
      if (!isWeb) return onFinish && onFinish(userId)
      isWeb && document.location.reload(true)
    }
  }

  return pug`
    Div.root
      Div.inputNameBlock
        Input.inputField( 
          type='text'
          label='Enter your nickname'
          placeholder='Your nickame...'
          value=data.userName
          onChangeText= text => $data.set('userName', text)
        )
        if errors['userName']
          Span.error #{errors['userName']}
        Input.inputField( 
          type='text'
          label='Enter your email'
          placeholder='Your email...'
          value=data.email
          onChangeText= text => $data.set('email', text)
        )
        if errors['email']
          Span.error #{errors['email']}
        Input.inputField( 
          type='text'
          label='Enter your password'
          placeholder='Your password...'
          value=data.password
          onChangeText= text => $data.set('password', text)
        )
        if errors['password']
          Span.error #{errors['password']}
      Div.buttonBlock
        Button.controlButton(onPress=register) Register
  `
})

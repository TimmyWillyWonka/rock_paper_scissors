import React from 'react'
import { observer } from 'startupjs'
import { BASE_URL } from '@env'
import './index.styl'
import { Div, Span } from '@startupjs/ui'
import { Image } from 'react-native'

const DATA = {
  icons: ['/icons/rock.png', '/icons/paper.png', '/icons/scissors.png']
}

export default observer(function () {
  return pug`
    Div.root
      Div.headerBlock
        Span.heading ROCK-PAPERS-SCISSORS
        Div.imageContainer
          each icon, index in DATA.icons
            Div.iconBlock(styleName={lastEl: index === 2})
              Image.icon(source={ uri: BASE_URL + icon })
  `
})

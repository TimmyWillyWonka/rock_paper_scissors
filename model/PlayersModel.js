import { BaseModel } from 'startupjs/orm'

export default class PlayersModel extends BaseModel {
  async addPlayer (data) {
    const id = this.id()
    await this.add({
      ...data,
      id,
      createdAt: Date.now()
    })
    return id
  }
}

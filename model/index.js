import GamesModel from './GamesModel'
import GameModel from './GameModel'
import PlayersModel from './PlayersModel'
import PlayerModel from './PlayerModel'

export default function (racer) {
  racer.orm('games', GamesModel)
  racer.orm('games.*', GameModel)
  racer.orm('players', PlayersModel)
  racer.orm('players.*', PlayerModel)
}

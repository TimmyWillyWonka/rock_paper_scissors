import { BaseModel } from 'startupjs/orm'

export default class GamesModel extends BaseModel {
  async addGameRoom (data) {
    const id = this.id()
    const { round, playerIds, start, finish, creatorId, name } = data
    await this.add({
      id,
      createdAt: Date.now(),
      round,
      playerIds,
      start,
      finish,
      creatorId,
      name
    })
    return id
  }
}

import { BaseModel } from 'startupjs/orm'

export default class GameModel extends BaseModel {
  async deleteGame () {
    this.del()
  }

  async joinToGame (user) {
    const game = this.get()
    if (game.creatorId !== user.id) {
      if (!game.playerIds.find(id => id === user.id)) {
        if (game.playerIds.length < 2) {
          this.push('playerIds', user.id)
        } else return false
      }
    }
    if (!user.isCreator) {
      const $players = this.root.at('players')
      await $players.addPlayer({
        userId: user.id,
        gameId: game.id,
        name: user.name,
        choice: 0,
        isWinner: false,
        roundWinner: false,
        combo: 0,
        history: [],
        move: false,
        score: 0
      })
    }
    return true
  }
}

import init from 'startupjs/init'
import orm from '../model'
import startupjsServer from 'startupjs/server'
import api from './api'
import getMainRoutes from '../main/routes'
import { initApp } from 'startupjs/app/server'
import { auth } from '@dmapper/auth/server'
import loginOptions from './loginOptions'
import passport from 'passport'
import hooks from './hooks'

// Init startupjs ORM.
init({ orm })

// Check '@startupjs/server' readme for the full API
const run = done => {
  startupjsServer({
    hooks,
    getHead,
    appRoutes: [
      ...getMainRoutes()
    ],
    beforeStart
  }, (ee, options) => {
    initApp(ee)

    ee.on('backend', async backend => {
      auth.init(backend, loginOptions)
    })
    ee.on('routes', expressApp => {
      expressApp.use('/api', api)
    })
    ee.on('afterSession', expressApp => {
      expressApp.use(passport.initialize())
      expressApp.use(passport.session())
      expressApp.use(auth.middleware())
    })
    ee.on('done', () => { done && done() })
  })
}

// One-time init right before we start to listen for incoming connections.
async function beforeStart (backend, cb) {
  cb && cb()
}

function getHead (appName) {
  return `
    <title>HelloWorld</title>
    <!-- Put vendor JS and CSS here -->
  `
}

export default run

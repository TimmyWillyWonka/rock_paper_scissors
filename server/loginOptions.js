import { LocalProvider } from '@dmapper/auth/server'
import bcrypt from 'bcrypt'

export default {
  strategies: {
    local: {
      emailRegExp: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
      hooks: {
        parseRegisterRequest: (req, res, done) => {
          const email = (req.body.email || '').toLowerCase()
          const password = req.body.password
          const confirm = req.body.confirm
          if (!email || !password || !confirm) {
            return done('Please fill in all required fields')
          }
          if (password !== confirm) {
            return done('Password should match confirmation')
          }
          if (!req.auth.options.strategies.local.emailRegExp.test(email)) {
            return done('Incorrect email')
          }
          if (password.length < 6) {
            return done('Password length should be at least 6')
          }
          // You can pass custom values to new user with help of userData parameter
          // For example we can pass userId from session
          const userData = { ...req.body.userData }
          done(null, email, password, userData)
        },
        register: async (req, options, email = '', password, userData, done) => {
          const { model } = req
          try {
            const profile = { email: email.toLowerCase(), ...userData }
            const salt = await bcrypt.genSalt(10)
            const hash = await bcrypt.hash(password, salt)
            profile.hash = hash
            profile.salt = salt
            const provider = new LocalProvider(model, profile, options)
            const authData = await provider.loadAuthData()
            if (authData) return done(options.errors.userExists)
            const userId = await provider.findOrCreateUser()
            // hack
            const $user = model.scope('users.' + userId)
            await $user.subscribe()
            await $user.setEach({ ...userData })
            done(null, userId)
          } catch (err) {
            done(err)
          }
        }
      }
    }
  }
}

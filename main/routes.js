function isLoggedIn (model, next, redirect) {
  const loggedIn = model.scope().get('_session.loggedIn')
  const redirectTo = '/registration'
  if (!loggedIn) return redirect(redirectTo)
  next()
}

function isNotLoggedIn (model, next, redirect) {
  const loggedIn = model.scope().get('_session.loggedIn')
  const redirectTo = '/'
  if (loggedIn) return redirect(redirectTo)
  next()
}

export default (components = {}) => [
  {
    path: '/',
    exact: true,
    component: components.PHome,
    filters: [isLoggedIn]
  },
  {
    path: '/registration',
    exact: true,
    component: components.PRegistration,
    filters: [isNotLoggedIn]
  },
  {
    path: '/create',
    exact: true,
    component: components.PCreateRoom,
    filters: [isLoggedIn]
  },
  {
    path: '/room/:gameId',
    exact: true,
    component: components.PGameRoom,
    filters: [isLoggedIn]
  }
]

import React from 'react'
import { observer } from 'startupjs'
import './index.styl'
import { Div } from '@startupjs/ui'
import Main from 'components/Main'

export default observer(function PHome () {
  return pug`
    Div.root
      Main
  `
})

import React from 'react'
import { observer } from 'startupjs'
import './index.styl'
import { Div } from '@startupjs/ui'
import CreateRoom from 'components/CreateRoom'

export default observer(function PCreateRoom () {
  return pug`
    Div.root
      CreateRoom
  `
})

export { default as PHome } from './PHome'
export { default as PRegistration } from './PRegistration'
export { default as PCreateRoom } from './PCreateRoom'
export { default as PGameRoom } from './PGameRoom'

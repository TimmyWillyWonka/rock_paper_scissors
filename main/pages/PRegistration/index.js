import React from 'react'
import { observer } from 'startupjs'
import './index.styl'
import { Div } from '@startupjs/ui'
import Registration from 'components/Registration'

export default observer(function PRegistration () {
  return pug`
    Div.root
      Registration
  `
})

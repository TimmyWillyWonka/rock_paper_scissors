import React from 'react'
import { observer } from 'startupjs'
import './index.styl'
import { Div } from '@startupjs/ui'
import Room from 'components/Main/Room'

export default observer(function PGameRoom ({ match: { params: { gameId } } }) {
  return pug`
    Div.root
      Room(gameId=gameId)
  `
})

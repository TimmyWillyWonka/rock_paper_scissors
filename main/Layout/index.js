import React from 'react'
import { observer } from 'startupjs'
import './index.styl'
import { Div } from '@startupjs/ui'
import Header from 'components/Header'

export default observer(function ({ children }) {
  return pug`
    Div.root
      Header
      Div.body= children
  `
})
